package com.tanitha.week11;

import com.tanitha.Bat;
import com.tanitha.Bird;
import com.tanitha.Human;
import com.tanitha.Superman;

public class App 
{
    public static void main( String[] args )
    {
        Bird bird1 = new Bird("Tweety");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();

        Plane boeing = new Plane("Boeing", "Rosaroi");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();

        Superman clark = new Superman("Clark");
        clark.takeoff();
        clark.fly();
        clark.landing();
        clark.walk ();
        clark.run();
        clark.eat();
        clark.walk();
        clark.swim();

        Human man1 = new Human("man");
        man1.eat();
        man1.sleep();
        man1.walk();
        man1.run();
        man1.swim();

        Bat bat1 = new Bat("Corona");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();

        Dog dog1 = new Dog("Boo");
        dog1.eat();
        dog1.sleep();
        dog1.swim();
        dog1.walk();
        dog1.run();

        
        Cat cat = new Cat("Somi");
        cat.eat();
        cat.sleep();
        cat.swim();
        cat.walk();
        cat.run();

        Rat rat = new Rat("ratty");
        rat.eat();
        rat.sleep();
        rat.swim();
        rat.walk();
        rat.run();

        Snake snake = new Snake("ZooZaa");
        snake.Crawl();
        snake.swim();
        snake.eat();
        snake.sleep();

        Crocodile crocodile = new Crocodile("Cookie");
        crocodile.Crawl();
        crocodile.swim();
        crocodile.eat();
        crocodile.sleep();
        crocodile.walk();
        crocodile.run();

        System.out.println();
        System.out.println("[ Group ]");
        Flyable[] flyables = {bird1, bat1, boeing, clark};
        System.out.println("---------group [Flyable]--------- ");
        for(int i =0; i < flyables.length; i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }

        Walkable[] walkables = {bird1, bat1, clark, man1, dog1, cat, rat, crocodile};
        System.out.println("---------group [Walkable]--------- ");
        for(int i =0; i < walkables.length; i++) {
            walkables[i].walk();
            walkables[i].run();
        }

        Swimable[] swimables = {clark, man1, dog1, cat, rat, crocodile};
        System.out.println("---------group [Swimable]--------- ");
        for(int i =0; i < swimables.length; i++) {
            swimables[i].swim();
        }

        Crawlable[] crawlables = {snake, crocodile};
        System.out.println("---------group [crawlables]--------- ");
        for(int i =0; i < crawlables.length; i++) {
            crawlables[i].Crawl();
            
        }

    }
}
