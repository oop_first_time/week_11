package com.tanitha.week11;

import com.tanitha.Animal;

public class Rat extends Animal implements Walkable,Swimable {

    public Rat(String name) {
        super(name, 4);
    }

    @Override
    public String toString() {
        return "Rat (" + this.getName() + ")";
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep");
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat");
    }

    @Override
    public void swim() {
        System.out.println(this + " swim");
    }

    @Override
    public void walk() {
        System.err.println(this + "walk");
    }

    @Override
    public void run() {
        System.out.println(this + "run");
        
    }
    
}
