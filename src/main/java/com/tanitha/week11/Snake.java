package com.tanitha.week11;

import com.tanitha.Animal;

public class Snake extends Animal implements Swimable,Crawlable {

    public Snake(String name) {
        super(name, 0);
    }

    
    @Override
    public void Crawl() {
        System.out.println(this + " crawl");
        
    }

    @Override
    public void swim() {
        System.out.println(this + " swim");
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep");
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat");
    }

    @Override
    public String toString() {
        return "Snake (" + this.getName() + ")";
    }  
}
