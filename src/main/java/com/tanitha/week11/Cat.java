package com.tanitha.week11;

import com.tanitha.Animal;

public class Cat extends Animal implements Walkable,Swimable {

    public Cat(String name) {
        super(name, 4);
       
    }

    @Override
    public String toString() {
        return "Cat (" + this.getName() + ")";
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep");
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat");
    }

    @Override
    public void swim() {
        System.out.println(this + " swim");
    }

    @Override
    public void walk() {
        System.err.println(this + "walk");
    }

    @Override
    public void run() {
        System.out.println(this + "run");
        
    }
      
}
