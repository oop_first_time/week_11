package com.tanitha.week11;

import com.tanitha.Animal;

public class Crocodile extends Animal implements Swimable,Walkable,Crawlable  {

    public Crocodile(String name) {
        super(name, 4);
    }
    @Override
    public void Crawl() {
        System.out.println(this + " crawl");
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep");
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat");
    }

    @Override
    public void swim() {
        System.out.println(this + " swim");
    }

    @Override
    public void walk() {
        System.err.println(this + "walk");
    }

    @Override
    public void run() {
        System.out.println(this + "run");
        
    }

    @Override
    public String toString() {
        return "Crocodile (" + this.getName() + ")";
    }
    
}
